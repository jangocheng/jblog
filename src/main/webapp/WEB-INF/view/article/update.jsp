<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language="java" import="java.util.List" %>
<%@ page language="java" import="com.newflypig.jblog.model.Category" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/common.css">
<title>分类修改</title>
</head>
<body>
	<form method="post">
		<input type="text" value="${article.title }" name="title" /><br />
		<script id="container" name="text" type="text/plain">${article.text }</script><br/>
		<script type="text/javascript" src="<%=request.getContextPath()%>/resources/ueditor/ueditor.config.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/resources/ueditor/ueditor.all.js"></script>
		<script type="text/javascript">
			var editor = UE.getEditor('container');	
		</script>
		<%
			List<Category> categories = (List<Category>) request.getAttribute("categories");
			for (Category c : categories) {
		%>
		<input type="checkbox" value="<%=c.getCategoryId()%>" name="ctgrs" <%=c.isChecked()?"checked=\"checked\"":""%>"/><span class="category"><%=c.getTitle()%></span>&nbsp;&nbsp;
		<%
			}
		%>
		<br/>
		<input type="submit" value="提交" />
		<a href="<%=request.getContextPath()%>/article/articles">返回文章列表</a>
	</form>
</body>
</html>