package com.newflypig.jblog.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Category entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "category", catalog = "jblog")
public class Category implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8213501951560245373L;
	// Fields

	private Integer categoryId;
	private String title;
	private Integer number=0;
	private Integer type=1;
	private Set<Article> articles = new HashSet<Article>(0);
	private boolean checked=false;

	// Constructors

	/** default constructor */
	public Category() {
	}

	/** full constructor */
	public Category(String title, Integer number, Integer type, Set<Article> articles) {
		this.title = title;
		this.number = number;
		this.type = type;
		this.articles = articles;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "category_id", unique = true, nullable = false)
	public Integer getCategoryId() {
		return this.categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	@Column(name = "title", length = 45)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "number")
	public Integer getNumber() {
		return this.number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	@Column(name = "type")
	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@ManyToMany
	@JoinTable(name="article_category",
		joinColumns={@JoinColumn(name="category_id")},
		inverseJoinColumns={@JoinColumn(name="article_id")}
	)
	public Set<Article> getArticles() {
		return this.articles;
	}

	public void setArticles(Set<Article> articles) {
		this.articles = articles;
	}

	@Transient
	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	@Override
	public boolean equals(Object c) {
		if(c instanceof Category){
			if(this.getCategoryId()==((Category)c).getCategoryId())
				return true;
			else
				return false;
		}else
			return false;
	}
}