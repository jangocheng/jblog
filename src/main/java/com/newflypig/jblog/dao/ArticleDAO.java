package com.newflypig.jblog.dao;

import java.util.List;

import com.newflypig.jblog.model.Article;

/**
 *	定义关于Article类的数据库特别操作
 *	time：2015年11月28日
 *
 */
public interface ArticleDAO extends BaseDAO<Article>{
	/**
	 * 倒序排列所有文章
	 * @return 倒序排列的Article有序列表
	 */
	public List<Article> findAllDesc();	
}
