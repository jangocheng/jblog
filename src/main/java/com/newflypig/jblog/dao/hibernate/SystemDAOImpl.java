package com.newflypig.jblog.dao.hibernate;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.newflypig.jblog.dao.SystemDAO;
import com.newflypig.jblog.model.System;

@Repository("systemDao")
public class SystemDAOImpl extends BaseHibernateDAO<System> implements SystemDAO{

	@Override
	public System loginByUsernamePwd(String userName, String password) {
		Query query=this.getSession().createQuery("from System s where s.userName=? and s.password=password(?)");
		query.setString(0, userName);
		query.setString(1, password);
		System system=(System)query.uniqueResult();
		if(system==null)
			log.info("Login Fail,userName:"+userName+" password:"+password);
		else
			log.info("Login Success");
		return system;
	}

	@Override
	public System getSystem() {
		Query query=this.getSession().createQuery("from System");
		query.setMaxResults(1);
		System system=(System) query.uniqueResult();
		if(system==null)
			log.info("getSystem fail");
		else
			log.info("getSystem Success");
		return system;
	}
}