package com.newflypig.jblog.dao.hibernate;

import org.springframework.stereotype.Repository;

import com.newflypig.jblog.dao.CommentDAO;
import com.newflypig.jblog.model.Comment;

@Repository("commentDao")
public class CommentDAOImpl extends BaseHibernateDAO<Comment> implements CommentDAO{
	// property constants
	public static final String NICKNAME = "nickname";
	public static final String TEXT = "text";
}