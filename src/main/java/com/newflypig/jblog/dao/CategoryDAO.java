package com.newflypig.jblog.dao;

import com.newflypig.jblog.model.Category;

/**
 *  定义关于Category类的数据库特别操作
 *	time：2015年11月28日 
 *
 */
public interface CategoryDAO extends BaseDAO<Category>{

}
