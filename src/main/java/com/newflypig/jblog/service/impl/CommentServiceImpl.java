package com.newflypig.jblog.service.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.newflypig.jblog.dao.BaseDAO;
import com.newflypig.jblog.dao.CommentDAO;
import com.newflypig.jblog.model.Comment;
import com.newflypig.jblog.model.Pager;
import com.newflypig.jblog.service.CommentService;
import com.newflypig.jblog.util.BlogConfig;

@Service("commentService")
public class CommentServiceImpl extends BaseServiceImpl<Comment> implements CommentService{

	@Autowired
	private CommentDAO commentDao;
	
	@Override
	protected BaseDAO<Comment> getDao() {
		return this.commentDao;
	}
	
	@Override
	public Pager<Comment> generatePage(DetachedCriteria dc, DetachedCriteria dc2, Integer page) {
		Pager<Comment> pagerComment = super.generatePage(dc, dc2, page);
		if(pagerComment==null)
			return null;
		
		//添加评论楼层的业务逻辑
		//floor=当前page*每页size+index
		int index=1;
		for(Comment c : pagerComment.getData()){
			c.setFloor((pagerComment.getCurrentPage()-1)*BlogConfig.getNumberPerPage(Comment.class)+index);
			index++;
		}
		
		return pagerComment;
	}
}
