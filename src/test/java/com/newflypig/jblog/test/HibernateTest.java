package com.newflypig.jblog.test;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

import com.newflypig.jblog.dao.hibernate.ArticleDAOImpl;
import com.newflypig.jblog.dao.hibernate.CategoryDAOImpl;
import com.newflypig.jblog.dao.hibernate.CommentDAOImpl;
import com.newflypig.jblog.dao.hibernate.SystemDAOImpl;
import com.newflypig.jblog.model.Article;
import com.newflypig.jblog.model.Category;
import com.newflypig.jblog.model.Comment;

@SuppressWarnings("unused")
public class HibernateTest {
	//@Test
	public void testFind() {
		Article a = new ArticleDAOImpl().findById(1);
		for (Category c : a.getCategories()) {
			System.out.println(c.getTitle());
		}
	}
	
	
	//@Test
	public void testAddCategory(){
		Article a = new ArticleDAOImpl().findById(1);
		for (Category c : a.getCategories()) {
			System.out.println(c.getTitle());
		}
		
		Category c = new Category();
		c.setTitle("J2EE");
		
		Comment comment=new Comment();
		comment.setNickname("newflypig");
		comment.setText("第二条评论");
		
		a.getCategories().add(c);
		comment.setArticle(a);
		
		ArticleDAOImpl ad = new ArticleDAOImpl();
		ad.getSession().getTransaction().begin();
		new CategoryDAOImpl().save(c);
		new CommentDAOImpl().save(comment);
		ad.update(a);
		ad.getSession().getTransaction().commit();
	}
	
	//@Test
	public void testAddArticle(){
		Article a=new Article();
		a.setTitle("一篇新的blog，编写于"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		a.setText("<div>今天是"+new SimpleDateFormat("yyyy年MM月dd号").format(new Date())+"，我在写blog</div>");
		
		ArticleDAOImpl ad=new ArticleDAOImpl();
		ad.getSession().getTransaction().begin();
		ad.save(a);
		ad.getSession().getTransaction().commit();
	}
}
